package es.cipfpbatoi;

import java.sql.SQLException;
import java.util.List;

import es.cipfpbatoi.dao.ClienteDAO;
import es.cipfpbatoi.modelo.Cliente;

public class App {
	
	// Requiere que esté instalado el componente "componentedaoclientes" en el repositorio local
	// para poder funcionar correctamente
	// Actividad: genera los componentes para poder gestionar artículos (articulodao y grupodao)
	// y, a continuación, enlaza dichos componentes a este proyecto.
	
	public static void main(String[] args) throws SQLException {
		Cliente cli = new Cliente("Nuevo", "C/ Nueva, 10");
		ClienteDAO cliDao = new ClienteDAO();
		if (cliDao.insert(cli)!=null) {
			System.out.println("Insertado: " + cli);
		}		

		List<Cliente> listaClientes = cliDao.findAll();
		for (Cliente cliente : listaClientes) {
			System.out.println(cliente);
		}
	}

}
